<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public $uses = array('Sensors', 'User', 'Triggers');
    
    
    public function beforeFilter() {
    parent::beforeFilter();
    // Allow users to register and logout.
   //$this->Auth->allow('add', 'logout');
    }
    
    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null) {
        $this->User->id = $id;
        print_r($id);
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->findById($id));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'trigger'));
            }
            $this->Flash->error(
                __('The user could not be saved. Please, try again.')
            );
        }
    }

    public function edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(
                __('The user could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function trigger(){
        
        $count = $this->User->find('count');
        $id = $this->User->find('all', array ('conditions' => array( 'id' => $count )));
        
        $i = $id[0]['User']['id'];
        
        if (    isset($this->request->data['Trigger']['Module de Young:']) 
                && isset($this->request->data['Trigger']['Longeur de la structure:'])
                && isset($this->request->data['Trigger']['Id Lora:'])){
            
            $L = $this->request->data['Trigger']['Longeur de la structure:'];
            $M = $this->request->data['Trigger']['Module de Young:'];
            pr($id_lora = $this->request->data['Trigger']['Id Lora:']);
        
            $this->Triggers->addCriticalTrigger($L, $M, $i, $id_lora);
            return $this->redirect(array('action' => '../Lora/index'));
            
            
        }else{
            pr("Please fill in all the fields!");
        }
    }
    
    public function delete($id = null) {
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');

        $this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }
    
    

public function login() {
    if ($this->request->is('post')) {
        if ($this->Auth->login()) {
            return $this->redirect($this->Auth->redirectUrl("../Lora/index"));
        }
        $this->Flash->set('Invalid username or password, try again');
    }
}

public function logout() {
    return $this->redirect($this->Auth->logout());
}
    

}

?>
