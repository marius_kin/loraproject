<?php

App::uses('AppController', 'Controller');
App::uses('File', 'Utility');
App::uses('Security', 'Utility');
App::uses('HttpSocket', 'Network/Http');

class LoraController extends AppController {

    public $uses = array('Sensors', 'User', 'Triggers');
    public $components = array('Session');

    public function index($id = null) {
        
       
    }
    
    public function dashboard() {
        
               
        
        $data = $this->Triggers->getTriggersFormUsr($this->Auth->user('id'));
        $count = count($data);
        $new_array = array();
        for($i = 0; $i<$count; $i++){
        
            $new_Sensor = $this->Sensors->getLastSensorFromId($data[$i]['Triggers']['lora_id']);
            
            if($new_Sensor['Sensors']['deformation'] >= $data[$i]['Triggers']['deformation']){
                array_push($new_array, $data[$i]);
            }
        
        }
        //$this->Triggers->getTriggers($usr_id);  
        
        $this->set('mes_triggers', $new_array);
        //$this->set('mes_triggers', $this->Triggers->find('all', 
        //           array('conditions'=>array('user'=>$this->Auth->user('id')))));
    }

    public function form_seuil() {
        
    }
    
    public function test() {
        
    }

    public function login() {
        
    }

    public function triggers()
    {
        
        if( isset($_GET["id"]) && isset($_GET["name"]) && isset($_GET["comment"]) 
                && $_GET["id"]!='' && $_GET["name"]!='' && $_GET["comment"]!='' ){
        
            pr($deformation = $_GET["minbeds"]);
            pr($lora_id = $_GET["id"]);
            pr($name = $_GET["name"]);
            pr($comment = $_GET["comment"]);
            pr($usr_id = $this->Auth->user('id'));
        
            $this->Triggers->addTrigger($usr_id, $lora_id, $name, $comment, $deformation);
        
        }else{
            pr("Make sure that the fields are not empty!");
        }
        
    }
    
    public function locations() {
        
    }

    public function tables_1(){
        
        $this->set('all', $this->Sensors->find('all'));
        $this->set('length', $this->Sensors->find('count'));
    }

    public function morris() {
        
    }

    public function module() {

        $this->set('New_Value');
        $HttpSocket = new HttpSocket();

        
            
        

        $results = $HttpSocket->get('http://ppecarl.esy.es/freq.txt', 'q=cakephp');
        
        $Current_frequecy = $results['body'];
        $this->set('Current_frequecy', $Current_frequecy);
        
        if( isset($this->request->data['New_Value']['Time_interval'])){
            
            $res = $this->request->data['New_Value']['Time_interval'];
            $results = $HttpSocket->post('http://ppecarl.esy.es/update_freq.php?freq=' . "$res");
            
            $message = "The time interval has been updated to ". "$res" . " seconds, Please click here to get redirected.";
            $this->Flash($message, '../../../Lora/', 1);
            
            
            
        }
        
    }

    public function morris_1() {

        $SensorIds = $this->Sensors->getAllSensorsIds();
        $this->set('SensorIds', $this->Sensors->getAllSensorsIds());
        $this->set('all', $this->Sensors->find('all'));
        $this->set('length', $this->Sensors->find('count'));

        if (isset($this->request->data['SensorId'])) {

            $id_lora = $SensorIds[$this->request->data['SensorId']['Id:']];
            $from_year = $this->request->data['SensorId']['From_year']['year'];
            $until_year = $this->request->data['SensorId']['Until_year']['year'];

            $from_month = $this->request->data['SensorId']['From_month']['month'];
            $until_month = $this->request->data['SensorId']['Until_month']['month'];

            $from_day = $this->request->data['SensorId']['From_day']['day'];
            $until_day = $this->request->data['SensorId']['Until_day']['day'];

            $date1 = "$from_year-$from_month-$from_day";
            $date2 = "$until_year-$until_month-$until_day";


            $this->set('id_lora', $id_lora);

            //print_r($id_lora);
            $this->set('BatteryLevel', $this->Sensors->getSensorsHumidityLevel($id_lora));


            $Sensors_Selected = $this->Sensors->getSensorsFromUntil($id_lora, $date1, $date2);

            $Sendor_Count = count($Sensors_Selected);


            $this->set('all', $Sensors_Selected);
            $this->set('length', $Sendor_Count);
        }
        
        
    }
    
    public function drop_triggers(){
        
        $data = $this->Triggers->find(
                'all', array('conditions' => 
                    array( 'user' => $this->Auth->user('id'))));
        
        $this->set('my_triggers',$data );
     
        
    }

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
}

?>
