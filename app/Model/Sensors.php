<?php

App::uses('AppModel', 'Model');

class Sensors extends AppModel {

    public $uses = array('Sensors');

    function checkSensor($eventId) {
        $datas = $this->read(null, $eventId);
        return $datas['Sensors']['id_lora'];
    }

    function getAllSensorsIds() {
        $datas = $this->find('list', array(
            'fields' => array('Sensors.id_lora')));

        $result = array_unique($datas);
        return $result;
    }

    function getSensorsFromUntil($id_lora, $from, $to) {



        $datas = $this->find('all', array(
            'conditions' => array(
                'Sensors.id_lora =' => $id_lora,
        )));

        
        foreach ($datas as $key1) {
            foreach ($key1 as $key){
                
                
                $originalDate = $key['date'];
                $newDate = date("Y-m-d", strtotime($originalDate));
                
                
                
            }
        }
        return $datas;
    }

    function getSensorsHumidityLevel($id_lora) {
        $datas = $this->find('first', array('conditions' => array('Sensors.id_lora =' => $id_lora),
            'order' => array('Sensors.date' => 'desc')
        ));
        
        $datas2 = $this->find('all', array('conditions' => array('Sensors.id_lora =' => $id_lora),
            'order' => array('Sensors.date' => 'desc')
        ));
        
        $count = $this->find('count', array('conditions' => array('Sensors.id_lora =' => $id_lora),
            'order' => array('Sensors.date' => 'desc')
        ));

        return ($datas2[$count - 1]['Sensors']['humidite']);
    }
    
    function getSensorsID_fromIdl_fromDef($id_lora, $h_or_e_deformation){
        
        $data = $this->find('all', array('conditions' => array(
            'id_lora' => $id_lora,
            'deformation >=' => $h_or_e_deformation
        )));
        $count = $this->find('count', array('conditions' => array(
            'id_lora' => $id_lora,
            'deformation >=' => $h_or_e_deformation
        )));
        
        
        $new_array = array();
        
        for($i = 0; $i<$count; $i++){
            array_push($new_array, $data[$i]['Sensors']['id_lora']);
        }
        
        $result = array_unique($new_array);
        
        return $result;
        }
        
        function getLastSensorFromId($id_lora){
            $data = $this->find('all', array('conditions' => array('id_lora' => $id_lora)));
            $count = count($data);
            if($count != 0){
            return $data[$count - 1];
            }
        }
        
        
                
        function sensor_passed_deformation($id_lora, $h_or_e_deformation){
            $datas = $this->find('all', array( 'conditions' => array(
                'id_lora' => $id_lora,
                'deformation >=' => $h_or_e_deformation 
            )));
            
            $count = count($datas);
            
            
            
        }

}



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

