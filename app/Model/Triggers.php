<?php

App::uses('AppModel', 'Model');

class Triggers extends AppModel {

    public $uses = array('Triggers');


    function getAllTriggersIds() {
        $datas = $this->find('list', array(
            'fields' => array('Triggers.id')));

        $result = array_unique($datas);
        return $result;
    }

    
    function addTriggerCritical($longeur, $E ){
        
        
    }
    
    function CriticalVaue($l, $e){
        return (15/$e)*$l;
    }
            
    
    function  addCriticalTrigger($l, $e, $id, $lora_id){
        
        
        $count = $this->find('count');
        $data = array
        (
                    'id' => $count + 1,
                    'user' => $id,
                    'lora_id' => $lora_id,
                    'gravite' => 0,
                    'nom' => 'Initial',
                    'description' => 'Critical',
                    'date' => date('Y-m-d'),
                    'heure' => date('H:i'),
                    'vue' => 0,
                    'deformation' =>  $this->CriticalVaue($l, $e)
        );
        
        $this->create('Triggers');
        $this->save($data);
        
        
    }
    
    function getTrigger($id){
        
        $datas = $this->find('all', array('conditions' => array( 'user' => $id )));
        
        return $datas;
    }
    
    function addTrigger($usr_id, $lora_id, $name, $comment, $deformation){
        
        $count = $this->find('count');
        $data = array
        (
                    'user' => $usr_id,
                    'lora_id' => $lora_id,
                    'gravite' => 1,
                    'nom' => $name,
                    'description' => $comment,
                    'date' => date('Y-m-d'),
                    'heure' => date('H:i'),
                    'vue' => 0,
                    'deformation' =>  $deformation
        );
        
        $this->create('Triggers');
        $this->save($data);
    }
    
    function deleteTrigger($usr_id, $name){
        $datas = $this->find('first', array('conditions' => array( 'user' => $usr_id,
                                                         'nom' => $name)));
                pr($datas);
    }
    
    function getTriggersFormUsr($usr_id){
        
        $data = $this->find('all', array('conditions', array(
            'user' => $usr_id
        )));
        return $data;
    }
}