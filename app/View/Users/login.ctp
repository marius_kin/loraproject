


<!-- cakephp -->


<div class="container">
    <form class="form" action="" id="UserLoginForm" method="post" accept-charset="utf-8">
        <div style="display:none;">
            <input type="hidden" name="_method" value="POST"/>
        </div> 
        <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please Sign In</h3>
                </div>
        <fieldset>
            <div class="form-group">
            <div class="input text required">
                <label for="UserPassword" style="margin-top: 1em">User name</label>
                <input name="data[User][username]" class="form-control" maxlength="50" type="text" id="UserUsername" required="required"/>
            </div>
                </div>
            <div class="input password required">
                <label for="UserPassword">Password</label>
                <input name="data[User][password]" class="form-control" type="password" id="UserPassword" required="required"/>
            </div>    
        
            <div class="submit" style="margin-top: 2em">
            <input class="btn btn-lg btn-primary btn-block" type="submit" value="Login"/>
        </div>
    </form>
</div>
        </div>
        </div>
        </div>
</body>
</html>