<?php
$this->layout = 'main';
?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Bootstrap Admin Theme</title>

        <!-- Bootstrap Core CSS -->
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Charts</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        echo $this->Form->create('SensorId', array(
                            'class' => 'form-horizontal',
                            'role' => 'form',
                            'inputDefaults' => array(
                                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                                'div' => array('class' => 'form-group'),
                                'class' => array('form-control'),
                                'label' => array('class' => 'col-sm-1 control-label'),
                                'between' => '<div class="col-lg-3">',
                                'after' => '</div>',
                                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                        )));


                        echo $this->Form->input('Id:', array('options' => $SensorIds,));
                                ?>


                        <div class="form-inline" style="margin-bottom: 1em;">

                            <?php
                            $options = array(
                                    'class' => 'form-control',
                                    'style' => 'margin-right: 1em',
                                    'div' => array('class' => 'col-xs-3',));
                            
                            echo '<div class="col-xs-2"><strong>Date From: </strong></div>';
                            echo $this->Form->year('From_year', 2016, date('Y'), $options);
                            echo $this->Form->month('From_month',$options);
                            echo $this->Form->day('From_day',$options);
                            ?>
                        </div>

                        <div class="form-inline">

                            <?php
                            echo '<div class="col-xs-2"><strong>Date Until: </strong></div>';
                            echo $this->Form->year('Until_year', 2016, date('Y'),$options);
                            echo $this->Form->month('Until_month',$options);
                            echo $this->Form->day('Until_day',$options);
                            ?>
                        </div>
                    </div>
                    <div class="row-border" style="padding-top: 5.5em; padding-bottom: 1em;">
                        <div class="pagination-sm">
                            <div class="text-center">

                                <?php
                                $options = array(
                                    'label' => 'Update',
                                    'class' => 'btn btn-default',
                                    
                                );

                                echo $this->Form->end($options);
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                Deformation
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div id="myfirstchart"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                Pression
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div id="mysecondchart"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                Temperature
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div id="mythirdchart"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                Humidity
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div id="myfourthchart"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                Donut Chart Example
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div id="donut-example"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Morris Charts JavaScript -->
        <script src="../bower_components/raphael/raphael-min.js"></script>
        <script src="../bower_components/morrisjs/morris.min.js"></script>
        <script src="../js/morris-data.js"></script>

        <script>

            new Morris.Line({
                // ID of the element in which to draw the chart.
                element: 'myfirstchart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                    //{day: '2012-3-16 16:20', value: 20, value2: 10},{day: '2013', value: 20, value2: 10}

<?php
$i = 0;

while (!empty($all[$i])) {

    if ($i != ($length - 1)) {
        echo "{day: " . "'" . $all[$i]['Sensors']['date'] ." ". $all[$i]['Sensors']['heure'] . "'" . ", value: " . $all[$i]['Sensors']['deformation'] . "}" . ",";
    } else {
        echo "{day: " . "'" . $all[$i]['Sensors']['date'] ." ". $all[$i]['Sensors']['heure']. "'" . ", value: " . $all[$i]['Sensors']['deformation'] . "}";
    }

    $i = $i + 1;
}
?>
                ],
                // The name of the data record attribute that contains x-values.
                xkey: 'day',
                // A list of names of data record attributes that contain y-values.
                ykeys: ['value'],
                // Labels for the ykeys -- will be displayed when you hover over the
                // chart.
                labels: ['Deformation']
            }
            );
        
        new Morris.Line({
                // ID of the element in which to draw the chart.
                element: 'mysecondchart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                    //{day: '2012-3-16 16:20', value: 20, value2: 10},{day: '2013', value: 20, value2: 10}

<?php
$i = 0;

while (!empty($all[$i])) {

    if ($i != ($length - 1)) {
        echo "{day: " . "'" . $all[$i]['Sensors']['date'] ." ". $all[$i]['Sensors']['heure'] . "'" . ", value: " . $all[$i]['Sensors']['pression'] . "}" . ",";
    } else {
        echo "{day: " . "'" . $all[$i]['Sensors']['date'] ." ". $all[$i]['Sensors']['heure']. "'" . ", value: " . $all[$i]['Sensors']['pression'] . "}";
    }

    $i = $i + 1;
}
?>
                ],
                // The name of the data record attribute that contains x-values.
                xkey: 'day',
                // A list of names of data record attributes that contain y-values.
                ykeys: ['value'],
                // Labels for the ykeys -- will be displayed when you hover over the
                // chart.
                labels: ['Pression']
            }
            );
        
        new Morris.Line({
                // ID of the element in which to draw the chart.
                element: 'mythirdchart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                    //{day: '2012-3-16 16:20', value: 20, value2: 10},{day: '2013', value: 20, value2: 10}

<?php
$i = 0;

while (!empty($all[$i])) {

    if ($i != ($length - 1)) {
        echo "{day: " . "'" . $all[$i]['Sensors']['date'] ." ". $all[$i]['Sensors']['heure'] . "'" . ", value: " . $all[$i]['Sensors']['temperature'] . "}" . ",";
    } else {
        echo "{day: " . "'" . $all[$i]['Sensors']['date'] ." ". $all[$i]['Sensors']['heure']. "'" . ", value: " . $all[$i]['Sensors']['temperature'] . "}";
    }

    $i = $i + 1;
}
?>
                ],
                // The name of the data record attribute that contains x-values.
                xkey: 'day',
                // A list of names of data record attributes that contain y-values.
                ykeys: ['value'],
                // Labels for the ykeys -- will be displayed when you hover over the
                // chart.
                labels: ['Temperature']
            }
            );
        
        new Morris.Line({
                // ID of the element in which to draw the chart.
                element: 'myfourthchart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                    //{day: '2012-3-16 16:20', value: 20, value2: 10},{day: '2013', value: 20, value2: 10}

<?php
$i = 0;

while (!empty($all[$i])) {

    if ($i != ($length - 1)) {
        echo "{day: " . "'" . $all[$i]['Sensors']['date'] ." ". $all[$i]['Sensors']['heure'] . "'" . ", value: " . $all[$i]['Sensors']['humidite'] . "}" . ",";
    } else {
        echo "{day: " . "'" . $all[$i]['Sensors']['date'] ." ". $all[$i]['Sensors']['heure']. "'" . ", value: " . $all[$i]['Sensors']['humidite'] . "}";
    }

    $i = $i + 1;
}
?>
                ],
                // The name of the data record attribute that contains x-values.
                xkey: 'day',
                // A list of names of data record attributes that contain y-values.
                ykeys: ['value'],
                // Labels for the ykeys -- will be displayed when you hover over the
                // chart.
                labels: ['Humidity']
            }
            );

            Morris.Donut({
                element: 'donut-example',
                data: [
                    {label: "Dryness %", value: <?php echo (100 - $BatteryLevel) ?>},
                    {label: "Humidity %", value: <?php echo $BatteryLevel ?>},
                ],
                colors: ['d9534f', '97E697']
            });




        </script>


    </body>

</html>
