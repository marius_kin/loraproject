

<!DOCTYPE html>
<html lang="en">

    <head>

        <?php
        $this->layout = 'main';
        ?>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Bootstrap Admin Theme</title>

        <!-- Bootstrap Core CSS -->
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link href="site-specifics.css" rel="stylesheet" type="text/css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        
        <title>Bootstrap 3 with Google Map</title>
                                <meta name="viewport" content="width=device-width, initial-scale=1.0">

                                <!-- Bootstrap core CSS -->
                                <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/css/bootstrap.css" rel="stylesheet" media="screen">

                                <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
                                <!--[if lt IE 9]>
                                  <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
                                  <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
                                <![endif]-->

                                <style>
                                    #map-container { height: 30em; width: 40em }

                                </style>

    </head>

    <body>



        <div id="wrapper">


            <!-- Page Content -->
            <div id="page-wrapper"> 
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Locations
                            </h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->

                    <div class="row">


                       
                                
                            
                            
                                <div id="map-container" class="col-md-6"></div>

                                <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
                                <!-- Include all compiled plugins (below), or include individual files as needed -->
                                <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
                                
                                <script>

                                    function init_map() {
                                        var var_location = new google.maps.LatLng(48.851901, 2.287084);

                                        var var_mapoptions = {
                                            center: var_location,
                                            zoom: 14
                                        };

                                        var var_marker = new google.maps.Marker({
                                            position: var_location,
                                            map: var_map,
                                            title: "Venice"});

                                        var var_map = new google.maps.Map(document.getElementById("map-container"),
                                                var_mapoptions);

                                        var_marker.setMap(var_map);

                                    }

                                    google.maps.event.addDomListener(window, 'load', init_map);

                                </script>
                            
                      


                    </div>

                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

    </body>

</html>
