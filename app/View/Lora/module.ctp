

<!DOCTYPE html>
<html lang="en">

    <head>

        <?php
        $this->layout = 'main';
        ?>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Bootstrap Admin Theme</title>

        <!-- Bootstrap Core CSS -->
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link href="site-specifics.css" rel="stylesheet" type="text/css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->


    </head>

    <body>



        <div id="wrapper">


            <!-- Page Content -->
            <div id="page-wrapper"> 
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>The frequency is set to <strong><?php echo $Current_frequecy;?></strong >. Please set the desired interval.</h4>
                        </div>
                        
                        <div class="col-lg-12">

                            

                            <!-- CakePHP -->
                            <form style="margin-top: 1em;" class="form-inline" action="/RepoGit/LoRaCake/Lora/module" id="New_ValueModuleForm" method="post" accept-charset="utf-8">
                                <div style="display:none;">
                                    <input type="hidden" name="_method" value="POST"/>
                                </div>
                                <div class="control-group">
                                    <label for="New_ValueTime_interval" class="control-label"><h4 class="h4">Time Interval: </h4></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"></div>
                                        <input name="data[New_Value][Time_interval]" class="form-control" type="text" id="New_ValueTime_interval" placeholder="Seconds"/>
                                        <div class="input-group-addon">sec</div>
                                    </div>
                                </div>
                                <div class="submit" style="margin-top: 1em;">
                                    <input class="btn btn-primary" type="submit" value="Update"/>
                                </div>
                            </form>


                            
                            <!-- CakePHP fin -->

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

    </body>

</html>
