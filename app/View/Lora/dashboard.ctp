


<!DOCTYPE html>
<html lang="en">

    <head>

   <?php 
    $this->layout = 'main'; 
   
    ?>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Bootstrap Admin Theme</title>

        <!-- Bootstrap Core CSS -->
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link href="site-specifics.css" rel="stylesheet" type="text/css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->


    </head>

    <body>
        <div id="wrapper">
            <!-- Page Content -->
            <div id="page-wrapper">
                <br>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                                <!-- /.col-lg-8 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Notifications Panel
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <?php
                                for($i=count($mes_triggers)-1;$i>=0;$i--)
                                {
                                ?>
                                <a href="#" class="list-group-item
                                   <?php 
                                   //if($mes_triggers[$i]['Triggers']['vue'] ==0)
                                   {
                                   if($mes_triggers[$i]['Triggers']['gravite'] == 0)
                                        echo "list-group-item-danger";
                                    else if($mes_triggers[$i]['Triggers']['gravite'] == 1)
                                        echo "list-group-item-warning";
                                    else if($mes_triggers[$i]['Triggers']['gravite'] == 2)
                                        echo "list-group-item-info";
                                    else
                                        echo "list-group-item-success";
                                   
                                   }?>">
                                    <i class="fa fa-<?php
                                    if($mes_triggers[$i]['Triggers']['gravite'] == 0)
                                        echo "warning";
                                    else if($mes_triggers[$i]['Triggers']['gravite'] == 1)
                                        echo "bolt";
                                    else if($mes_triggers[$i]['Triggers']['gravite'] == 2)
                                        echo "envelope";
                                    else
                                        echo "bell";
                                    ?> fa-fw"></i>
                                    <?php
;                                        echo  strtoupper($mes_triggers[$i]['Triggers']['nom']) .': '. strtolower($mes_triggers[$i]['Triggers']['description']);
                                    ?>
                                    <span class="pull-right text-muted small">
                                        <em>
                                            <?php 
                                            echo $mes_triggers[$i]['Triggers']['date'] ." à " .$mes_triggers[$i]['Triggers']['heure'];
                                            ?>
                                        </em>
                                    </span>
                                </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

    </body>

</html>



